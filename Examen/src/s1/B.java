package s1;

public class B {
	private String param;
	private D d;
	C c;
	private E e= new E();
	public B()
	{
		
	}
	public void x() {
		
	}
	public void y() {
		
	}
}

class A extends B {
	public A() {
		
	}
	@Override
	public void x() {
		
	}
	public void y() {
		
	}
}

class C {
	B b = new B();
	public C() {
		
	}
}

class D {
	public D() {
		
	}
}

class E {
	public E() {
		
	}
}

class U {
	public U(){
		
	}
}