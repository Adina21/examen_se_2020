package s2;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Threads extends Thread {
	
    Threads(String name){
        super(name);
  }

  public void run(){
        for(int i=0;i<7;i++){
            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
            Date date = new Date(System.currentTimeMillis());
              System.out.println(getName() +" - " + formatter.format(date));

              try {
                    Thread.sleep(2000);
              } catch (InterruptedException e) {
                    e.printStackTrace();
              }
        }
        System.out.println(getName() + " Done");
  }

  public static void main(String[] args) {
        Threads T1 = new Threads("CThread1");
        Threads T2 = new Threads("CThread2");

        T1.start();
        T2.start();
  }

}
